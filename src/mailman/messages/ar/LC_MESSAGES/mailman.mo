��          \      �       �      �      �      �        =   $     b     k  �  t  ,   m  4   �     �  "   �  U        f     v                                       Display Mailman's version. List all mailing lists. Original Message Unsubscription request Welcome to the "${mlist.display_name}" mailing list${digmode} [ADD] %s [DEL] %s Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-12-27 07:10+0000
Last-Translator: ButterflyOfFire <boffire@users.noreply.hosted.weblate.org>
Language-Team: Arabic <https://hosted.weblate.org/projects/gnu-mailman/mailman/ar/>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;
X-Generator: Weblate 5.4-dev
 اعرض نسخة Mailman (مايل مان). إظهار جميع القوائم البريدية. الرسالة الأصلية طلب إلغاء الاشتراك مرحباً بكم في قائمة البريد {digmode} ”{mlist.display_name}“ [إضافة] %s [حذف] %s 